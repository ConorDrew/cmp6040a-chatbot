from pyknow import *
from webScrap import *

from validation import *


# Selfmade class to move around KE
class Action(Fact):
    pass


class Greetings(KnowledgeEngine):
    # Checks the item that has the issue.
    def item_issue(self):
        string = issue_selection("chatbot: What part is incorrect? \nUser: ")
        self.declare(Fact(item=string))


    # Makes the date easier to read for the user
    def pretty_date(self, date):
        findDay = re.compile(r'\b((mon|tues|wed(nes)?|thur(s)?|fri|sat(ur)?|sun)(day)?)|today|tomorrow\b')
        matchDay = findDay.search(date)
        if matchDay:
            return date
        else:
            return date[0] + date[1] + "/" + date[2] + date[3] + "/" + date[4] + date[5]

    # Makes the time easier to read for the user
    def pretty_time(self, time):
        return time[0] + time[1] + ":" + time[2] + time[3]

    # Starting point
    @DefFacts()
    def _initial_action(self):
        yield Fact(action="get-info")
        yield Fact(action="start-up")

    #welcome message
    @Rule(Fact(action='start-up'),
          salience=10)
    def startup(self):
        print("Chatbot: I am going to help you find the cheapest train fair, before I start I need to get some information.")

    # Get destination
    @Rule(Fact(action='get-info'),
          NOT(Fact(destination=W())),
          salience=9)
    def destination(self):
        ref = input("Chatbot: Where would you like to go? \nUser: ").lower()
        self.declare(Fact(destination=ref))

    # Get departure
    @Rule(Fact(action='get-info'),
          NOT(Fact(departure=W())),
          salience=8)
    def departure(self):
        ref = input("Chatbot: Where are you departing from\nUser: ").lower()
        self.declare(Fact(departure=ref))

    # Get Date
    @Rule(Fact(action='get-info'),
          NOT(Fact(date=W())),
          salience=7)
    def date(self):
        ref = date_validation("Chatbot: What date do you want to book the ticket? \nUser: ")
        self.declare(Fact(date=ref))

    # Get time
    @Rule(Fact(action='get-info'),
          NOT(Fact(time=W())),
          Fact(date=MATCH.date),
          salience=6)
    def time(self, date):
        ref = time_validation("Chatbot: What time?? (HH:MM) \nUser: ", date)
        self.declare(Fact(time=ref))

    @Rule(Fact(action='get-info'),
          NOT(Fact(arrv=W())),
          salience=5)
    def dep_arrv(self):
        # if true, set to arrival
        data = arrive_depart("Chatbot: Would you like to Arrive or Depart at this time? \nUser: ")
        if data == "arrive":
            self.declare(Fact(arrv="arrive"))
        elif data == "depart":
            self.declare(Fact(arrv="depart"))
        elif data == "first":
            self.declare(Fact(arrv="first"))
        elif data == "last":
            self.declare(Fact(arrv="last"))

        self.declare(Action('check'))

    # Quality Check
    @Rule(Action('check'),
          Fact(action='get-info'),
          Fact(departure=MATCH.departure),
          Fact(destination=MATCH.destination),
          Fact(date=MATCH.date),
          Fact(time=MATCH.time),
          Fact(arrv=MATCH.arrv),
          AS.f3 << Action(MATCH.cc))
    def information_check(self, departure, destination, date, time, arrv, f3):
        self.retract(f3)

        if arrv == "arrive" or arrv == "depart":
            arr_output = " and " + arrv.upper()
        else:
            arr_output = " taking the " + arrv.upper() + " train"

        print("Chatbot: You want to go from: ", departure.upper(), " to ", destination.upper(),
              " on ", self.pretty_date(date), arr_output, " at ", self.pretty_time(time))
        if not yes_no("Chatbot: Is this correct? \nUser: "):
            #print("DEBUG:: ENTERED NO")
            #self.declare(Action('issue'))
            self.item_issue()
            self.declare(Action('issue'))
        else:
            self.declare(Action('get-url'))

        #print(self.facts)

        # clear all

    # clears rule if user needs to change item
    @Rule(Action('issue'),
          Fact(item=MATCH.item),
          AS.dest << Fact(destination=MATCH.destination),
          AS.dep << Fact(departure=MATCH.departure),
          AS.d << Fact(date=MATCH.date),
          AS.t << Fact(time=MATCH.time),
          AS.a << Fact(arrv=MATCH.arrv),
          AS.f4 << Fact(item=MATCH.item),
          AS.f3 << Action(MATCH.cc))
    def test_issue(self, item, dest, dep, d, t, a, f4, f3):

        #if item name matches, clear that fact
        if item == "destination":
            self.retract(dest)
        elif item == "departure":
            self.retract(dep)
        elif item == "date":
            self.retract(d)
        elif item == "time":
            self.retract(t)
        elif item == "arrive":
            self.retract(a)
        else:
            print("Chatbot: SOMETHING WENT WRONG")

        #clears action and item facts
        self.retract(f3)
        self.retract(f4)
        self.declare(Action('check'))

    #
    #  Print and get url
    #
    @Rule(Action('get-url'),
        Fact(departure=MATCH.departure),
        Fact(destination=MATCH.destination),
        Fact(date=MATCH.date),
        Fact(time=MATCH.time),
        Fact(arrv=MATCH.arrv),
        AS.f3 << Action(MATCH.cc))
    def get_url(self, departure, destination, date, time, arrv, f3):
        self.retract(f3)
        print("Chatbot: SEARCHING FOR TICKETS.....")

        trainList = getTable(destination, departure, date, time, arrv)

        listlen = len(trainList)

        if listlen == 0:
            print("Chatbot: Sorry, I couldn't find any results with the details you gave me, please try again")
        else:
            count = 0
            while True:
                if count == 0:
                    print("Chatbot: The cheapest ticked I found is: ", trainList[count][5])
                    print("Details: Departure time: ", trainList[count][0], "| From:",
                          trainList[count][1], "| To:", trainList[count][2], "| Arrival time:", trainList[count][3],
                          "| Duration:", trainList[count][4], "| Price:", trainList[count][5])
                else:
                    print("Chatbot: The next cheapest ticket is: ", trainList[count][5])
                    print("Details: Departure time: ", trainList[count][0], "| From:",
                          trainList[count][1], "| To:", trainList[count][2], "| Arrival time:", trainList[count][3],
                          "| Duration:", trainList[count][4], "| Price:", trainList[count][5])

                if yes_no("Chatbot: Would you like this link? \nUser: "):
                    print("Chatbot: ", getURL(destination, departure, date, time, arrv))
                    break
                else:
                    if yes_no("Chatbot: would you like the next cheapest? \nUser: "):
                        if count < listlen - 1:
                            count += 1
                            continue
                        else:
                            print("Chatbot: I'm sorry I don't have any more results, please try again.")
                            break
                    else:
                        break

            print("Chatbot: Thank you, have a safe trip.")


def bookingEngine():

    engine = Greetings()
    engine.reset() # Prepare the engine for the execution.
    engine.run() # Run it!
