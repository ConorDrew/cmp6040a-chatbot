from knowlegeEngine import bookingEngine
from delayedEngine import delayEngine

def help():
    print("Chatbot: This is what I can currently help you with ")
    print("Booking: I can help you find the cheapest train ticket that suits your needs")
    print("Delay: I can help estimate your time of arrival is you are delayed"
          " or if you would like to find out if a train will be delayed in the future")
    pass


def run():
    data = input("Chatbot: Hello, how may i help you?? \nUser: ").lower()
    bookingWords = ["book", "booking", "reserve"]
    delayWords = ["delay", "delayed"]
    negative = ["no", "goodbye", "bye", "leave", "exit", "quit"]
    intent = ""

    gotAnswer = False
    while True:
        if any(ext in data for ext in bookingWords):
            bookingEngine()
            gotAnswer = True
        elif any(ext in data for ext in delayWords):
            delayEngine()
            gotAnswer = True
        elif any(ext in data for ext in negative):
            print("Chatbot: Have a good day")
            break
        elif data == "help":
            help()
            gotAnswer = True
        else:
            print("Chatbot: Sorry, I currently have very limited skill sets.")

        if gotAnswer:
            data = input("Chatbot: Can I help you with anything else? ").lower()
        else:
            data = input("Chatbot: Please ask to do something different: ").lower()


run()