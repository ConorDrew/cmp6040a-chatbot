import requests
import numpy as np
from bs4 import BeautifulSoup

#imports for testing

import time as clockTime
from testingFile import timing
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 12:40:03 2019

@author: 许珂
"""

def getURL(destination, start, date, time, arrv):
    if arrv == "arrive":
        arrive_depart = "arr"
    elif arrv == "depart":
        arrive_depart = "dep"
    elif arrv == "first":
        arrive_depart = "first"
    elif arrv == "last":
        arrive_depart = "last"

    url = "http://ojp.nationalrail.co.uk/service/timesandfares/%s/%s/%s/%s/%s"\
          % (start, destination, date, time, arrive_depart)
    return str(url)

def getTable(destination, start, date, time, arrv):

    startTime = clockTime.clock()

    table = []
    #get the URL
    response = requests.get(getURL(destination, start, date, time, arrv))
    #print(getURL(destination, start, date, time, arrv))

    soup = BeautifulSoup(response.content, "html.parser")
    for tag in soup.find_all("span",{'class':'ctf-plat'}):
        tag.replaceWith('')

    mainSoup = soup.find('div', class_="otherFaresContainer")

    if mainSoup == None:
        t_s = []
    else:
        for i in range(0, 5):
            line = []

            #find the departure time
            dep = soup.find_all('td', class_='dep')
            dep1 = dep[i].get_text().strip()
            line.append(dep1)

            #find the origin
            origin = soup.find_all('td', class_='from')
            origin1 = origin[i].get_text().strip()
            line.append(origin1)

            #find the destination
            des = soup.find_all('td', class_='to')
            des1 = des[i].get_text().strip()
            line.append(des1)

            #find the arrive time
            arr = soup.find_all('td', class_='arr')
            arr1 = arr[i].get_text().strip()
            line.append(arr1)

            #find the time of duration
            dur = soup.find_all('td', class_='dur')
            dur1 = dur[i].get_text().replace("\n", "").replace("\t", "")
            line.append(dur1)

            #find the price
            fare = soup.find_all('label', class_='opsingle')
            fare1 = fare[i].get_text().strip()
            line.append(fare1)
            table.append(line)

        t = tuple(table)
        t_s = sorted(t, key=lambda t:t[5])

        endTime = clockTime.clock()
        #print(timing(startTime, endTime))

    return t_s