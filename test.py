n = int(input("The number is: "))
result = []
while n != 1: 
      result.append(n) 
      if n % 2 == 0:
          n //= 2
      else:
          n = n * 3 + 1
result.append(n) 

print(result)