"""

this will hold all validation data, taking input from the KB and formatting it to work
correctly with the information retreival

"""
import datefinder
import arrow
import re
import csv

def yes_no(prompt):
    while True:
        data = input(prompt).lower()
        if not (data == "yes" or data == "no"):
            print("Chatbot: Thats not a valid response to this question, please answer yes or no!")
            continue
        else:
            break

    if data == "yes":
        return True
    else:
        return False


# Arrive / Depart
def arrive_depart(prompt):
    arrive = ["arrive", "get there"]
    depart = ["depart", "leave", "go"]
    first = ["first", "first train"]
    last = ["last", "last train"]
    validAnswers = arrive + depart + first + last
    while True:
        data = input(prompt).lower()
        if data not in validAnswers:
            print("Chatbot: That's not a valid response to this question, please answer Arrive or Depart!")
            continue
        else:
            break

    if data in arrive:
        return "arrive"
    elif data in depart:
        return "depart"
    elif data in first:
        return "first"
    elif data in last:
        return "last"

# issue selection
def issue_selection(prompt):
    sections = ["departure", "dep", "destination", "dest", "time", "date", "arrival", "departure", "arrive", "depart"]

    while True:
        data = input(prompt).lower()
        if data not in sections:
            print("Chatbot: Thats not a valid section")
            continue
        else:
            break

    # switch statements for diffrent pos
    if data == "departure" or data == "dep":
        return "departure"
    if data == "destination" or data == "dest":
        return "destination"
    if data == "time":
        return "time"
    if data == "date":
        return "date"
    if data == "arrival" or data == "departure" or data == "arrive" or data == "depart":
        return "arrive"


def time_validation_simple(prompt):
    time = ""
    while True:
        data = input(prompt)
        matches = datefinder.find_dates(data)

        for match in matches:
            date = arrow.get(match)
            time = date.format('HHmm')

        if time == "":
            print("Chatbot: Thats not a valid time entry, Try again!")
            continue
        else:
            break;

    return time


def time_validation(prompt, date):
    findDay = re.compile(r'\b((mon|tues|wed(nes)?|thur(s)?|fri|sat(ur)?|sun)(day)?)|today|tomorrow\b')
    matchDay = findDay.search(date)
    dateIsString = False

    if matchDay:
        d = date
        dateIsString = True
        # check if date is today, if so dont let it go in the past like before.
    else:
        dateIsString = False
        d = arrow.get(date, 'DDMMYY')

    time = ""
    while True:
        data = input(prompt)
        matches = datefinder.find_dates(data)
        now = arrow.now()

        for match in matches:
            date = arrow.get(match)
            time = date.format('HHmm')

        if time == "":
            print("Chatbot: Thats not a valid time entry, Try again!")
            continue
        if dateIsString:
            if d == "today" and date.time() < now.time():
                print("Chatbot: The time you are selecting is in the past, Try again!")
            else:
                break
        else:
            if d.date() == now.date() and date.time() < now.time():
                print("Chatbot: The time you are selecting is in the past, Try again!")
                continue
            else:
                break
    return time


def date_validation(prompt):
    dateFormat = ""
    findDay = re.compile(r'\b((mon|tues|wed(nes)?|thur(s)?|fri|sat(ur)?|sun)(day)?)|today|tomorrow\b')


    while True:
        data = input(prompt).lower()
        now = arrow.get()
        isPast = False

        matchDay = findDay.search(data)

        if matchDay:
            dateFormat = matchDay.group(0)
        else:
            matches = datefinder.find_dates(data)

            for match in matches:
                date = arrow.get(match)

                if date.date() < now.date():
                    isPast = True
                    continue
                else:
                    if date.date().day < 13:
                        dateFormat = date.format('MMDDYY')
                    else:
                        dateFormat = date.format('DDMMYY')

        if isPast:
            print("Chatbot: You cant pick a date from the past, Try again!")
            continue
        elif dateFormat == "":
            print("Chatbot: That's not a valid time entry, Try again!")
            continue
        else:
            break
    return dateFormat


def delay_time(prompt):
    time = ""
    timeRegex = re.compile(r'^(?:(\d+)\s*(?:$|[mM]\w*|:)\s?([0-5]?\d)?)|(\d+)(?:\s*[hH]\w*)')

    while True:
        data = input(prompt).lower()

        findTime = timeRegex.search(data)

        if not findTime:
            print("Chatbot: Sorry I didnt understand, please try again \nUser: ")
        else:
            break
    time_out = re.sub("\D", "", findTime.group(0))

    return time_out

def find_station(station_in):
    station_out = ""
    with open('station_codes.csv', 'r') as f:
        reader = csv.reader(f)
        station_list = list(reader)

    for stations in station_list:
        if station_in in stations[0]:
            #print(stations[0], stations[1])
            station_out = stations[1]
            break

    return station_out

# 15:00 = 15 hours
# 00:15 = 15 mins
# 5 mins = 5 mins
# 1 hour = 1 hour
# 20 = 20 mins