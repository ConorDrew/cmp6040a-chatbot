import requests
import json
import arrow

headers = { "Content-Type": "application/json" }
auths = ("nnf15evu@uea.ac.uk", "Group16!")


def clock_loop(time):
    if time >= 2400:
        time -= 2400
    elif time <= 0:
        time += 2400

    return str(time).zfill(4)

def get_trains(from_loc, to_loc, date_from, date_now, time, day):
    weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

    from_time = int(time) - 100
    to_time = int(time) + 100

    from_time = clock_loop(from_time)
    to_time = clock_loop(to_time)

    if day in weekdays:
        new_day = "WEEKDAY"
    else:
        new_day = day.upper()

    #print("DEBUG::", from_loc, to_loc, from_time, to_time,date_from, date_now, new_day)

    api_url = "https://hsp-prod.rockshore.net/api/v1/serviceMetrics"
    data = {
        "from_loc": from_loc,
        "to_loc": to_loc,
        "from_time": from_time,
        "to_time": to_time,
        "from_date": date_from,
        "to_date": date_now,
        "days": new_day
    }

    r = requests.post(api_url, headers=headers, auth=auths, json=data)
    #print(json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',', ': ')))

    data = json.loads(r.text)
    rids_list = []

    for p in data['Services']:
        #print(p['serviceAttributesMetrics']['rids'])
        rids = p['serviceAttributesMetrics']['rids']
        rids_list = rids_list + rids

    return rids_list


def get_train(rid):
    api_url = "https://hsp-prod.rockshore.net/api/v1/serviceDetails"
    data = {
      "rid": rid
    }

    r = requests.post(api_url, headers=headers, auth=auths, json=data)
    #print(json.dumps(json.loads(r.text), sort_keys=True, indent=2, separators=(',', ': ')))

    data = json.loads(r.text)

    for p in data['serviceAttributesDetails']:
        for d in data['serviceAttributesDetails']['locations']:
            #print(d)
            trainDetails = d

    #print("Train should arrive at: ", trainDetails['gbtt_pta'], "Train did arrive at: ", trainDetails['actual_ta'])

    # Data Validation
    if trainDetails['gbtt_pta'] == '' or trainDetails['actual_ta'] == '':
        displayed_arrival = 0
        actual_arrival = 0
        delay_time = 0
    else:
        displayed_arrival = arrow.get(trainDetails['gbtt_pta'], 'HHmm')
        actual_arrival = arrow.get(trainDetails['actual_ta'], 'HHmm')

        time_subtracted = actual_arrival - displayed_arrival

        toFlip = False

        if time_subtracted.days == -1:
            # flip math to make positive
            time_subtracted = displayed_arrival - actual_arrival
            delay_time = arrow.get(str(time_subtracted), 'H:mm:ss')
            toFlip = True
        else:
            delay_time = arrow.get(str(time_subtracted), 'H:mm:ss')

        if delay_time.time().hour == 0:
            delay_time = int(delay_time.format('mm'))
        else:
            hours = delay_time.format('HH')
            mins = delay_time.format('mm')
            delay_time = (int(hours) * 60) + int(mins)

        if toFlip:
            delay_time = delay_time * (-1)

    return delay_time




