import arrow
from validation import find_station
from hsp_data import *
import math

#imports for testing

import time as clockTime
from testingFile import timing



def get_future_delay(from_loc, to_loc, date, time):
    # take in date that customer is planning on going, and see the last 3 months from last year.
    date_now = arrow.get(date, 'DDMMYY').shift(years=-1).format('YYYY-MM-DD')
    date_from = arrow.get(date_now, 'YYYY-MM-DD').shift(months=-1).format('YYYY-MM-DD')
    day = arrow.get(date_now).format('dddd')

    startTime = clockTime.clock()

    rids = get_trains(from_loc, to_loc, date_from, date_now, time, day)

    if not rids:
        print("Chatbot: Sorry, I didn't find any train data matching that")
    else:
        print("Chatbot: Calculating delay time, This may take a while, please wait...")

        delay = 0
        count = 0
        biggest_delay = 0
        smallest_delay = 0
        for item in rids:
            train_data = get_train(item)
            delay += train_data
            count += 1

            if train_data > biggest_delay:
                biggest_delay = train_data
            if train_data < smallest_delay:
                smallest_delay = train_data

            if count == 100:
                print("Chatbot: Still calculating, stay with me.")
            if count == 200:
                print("Chatbot: This is taking longer than I expected, there is lots of data here")
            if count == 300:
                print("Chatbot: Wow this is longer than the U.Ks train services.")

        print("Chatbot: Avg delay for this day is: ", math.floor(int(delay) / count), "minutes")
        print("Chatbot: The longest delay for this day was ", biggest_delay, "minutes")
        print("Chatbot: But the quickest time for this route was ", smallest_delay, "minutes")

        endTime = clockTime.clock()
        print(timing(startTime, endTime))




def get_current_delay(from_loc, to_loc, user_delay):
    # Take in todays date and minus 3 months
    date_now = arrow.now().format('YYYY-MM-DD')
    date_from = arrow.get(date_now, 'YYYY-MM-DD').shift(months=-1).format('YYYY-MM-DD')
    time_now = arrow.now().format('HHmm')
    day = arrow.get(date_now).format('dddd')

    startTime = clockTime.clock()

    rids = get_trains(from_loc, to_loc, date_from, date_now, time_now, day)

    if not rids:
        print("Chatbot: Sorry, I didn't find any train data matching that")

    else:

        print("Chatbot: Calculating delay time, This may take a while, please wait...")

        delay = 0
        count = 0

        for item in rids:
            train_data = get_train(item)
            delay += train_data
            count += 1

            if count == 100:
                print("Chatbot: Still calculating, stay with me.")
            if count == 200:
                print("Chatbot: This is taking longer than I expected, there is lots of data here")
            if count == 300:
                print("Chatbot: Wow this is longer than the U.Ks train services.")

        total_delay = math.floor(int(delay) / count) + int(user_delay)

        print("Chatbot: looking at previous train data and your current delay time, your delay will be around", total_delay)

        endTime = clockTime.clock()
        print(timing(startTime, endTime))
