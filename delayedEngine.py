from validation import find_station
from validation import delay_time
from validation import date_validation
from validation import time_validation_simple
import arrow

from preditionModel import *

def get_to_from():
    while True:
        data = input("Chatbot: What's the destination? \nUser: ").lower()

        destination = find_station(data)

        if destination == "":
            print("Chatbot: sorry, Station not found, try again.")
            continue
        else:
            break

    while True:
        data = input("Chatbot: Where would you like to depart from?\nUser: ").lower()

        depart_from = find_station(data)

        if depart_from == "":
            print("Chatbot: Station not found, try again.")
            continue
        else:
            break

    return [destination, depart_from]

def delayEngine():
    while True:
        #future delay or current delay tree
        delay_type = input("Chatbot: Are you currently delayed or would you like to know about a future delay? \nUser:")

        if "current" in delay_type:
            to_from = get_to_from()
            user_delay = delay_time("Chatbot: How long are you currently delayed by? \nUser:")
            #print("From", to_from[0], "To", to_from[1],"Delayed by", user_delay, "mins")
            get_current_delay(to_from[0], to_from[1], user_delay)
            break

        elif "future" in delay_type:
            to_from = get_to_from()
            date = date_validation("Chatbot: What date are you looking for?(DD/MM/YY) \nUser: ")
            #dateFormat = arrow.get(date, 'DDMMYY').format('YYYY-MM-DD')
            time = time_validation_simple("Chatbot: What time would you like to arrive?(HH:MM) \nUser: ")
            #print("From", to_from[0], "To", to_from[1],"Date", date, "Time", time)
            get_future_delay(to_from[0], to_from[1], date, time)
            break


        else:
            print("Chatbot: Sorry I didn't catch that")

